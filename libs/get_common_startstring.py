#! /usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from os.path import commonprefix


def main():
    arguments = sys.argv[1:]
    print(commonprefix(arguments))


if __name__ == "__main__":
    main()
