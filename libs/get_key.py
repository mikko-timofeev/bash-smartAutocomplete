#! /usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import subprocess
from ruamel.yaml import YAML


def clean_output(a_list):
    a_list.sort()
    if "--" in a_list:
        a_list.remove("--")  # exclude final argument filter
    # if '==' in a_list:
    #   a_list.remove('==')
    return "\n".join(a_list)


def key_list(a_dict):
    if not a_dict or isinstance(a_dict, str):
        return []

    return [*a_dict.keys()]


def dig(mp, pth):
    try:
        return dig(mp[pth[0]], pth[1:]) if pth else mp
    except (TypeError, KeyError):
        return "--"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(dest="filename", help="This is a file to parse")
    parser.add_argument(
        dest="path",
        help='This is a " // " separated path to read',
        nargs="?",
        default="",
    )

    args = parser.parse_args()
    with open(args.filename, "r") as f:
        yaml = YAML(typ="safe", pure=True)
        content = yaml.load(f)
        ypath = args.path  # full command, arguments separated with ' // '
        path = ypath.split(" // ")

        command_name = path.pop(0)
        param_or_value = ""

        # blank input
        if not command_name:
            res = key_list(content)
        else:
            key_map = content.get(command_name)

            all_keys = key_list(key_map)
            used_keys = [i for i in path if i in all_keys]

            # assign allowed keys
            allowed_keys = all_keys

            if "==" in key_map:
                allowed_keys = key_list(dig(key_map["=="], used_keys))

            res = []

            while not res:

                # suggest arguments
                if not path:
                    res = list(set(allowed_keys) - set(used_keys))
                    break
                else:
                    res = []
                    used_values = []  # parameter values that were supplied

                    enough = False
                    while len(path) > 0:

                        if param_or_value == "":
                            param_or_value = path[-1]

                        try:
                            d = content[command_name].get(param_or_value)

                            # get AttributeError if it is a value
                            used_keys.append(path[-1])

                            if not d.get(
                                "filter", {}
                            ):  # if dict is empty, it is a flag, not parameter
                                del path[-1]
                                break

                            match = "*"
                            if d.get("order"):
                                for orfil in d["order"]:  # ordered filters
                                    if bool(
                                        ypath.endswith(f"{orfil} // {param_or_value}")
                                    ):
                                        match = orfil
                                        break

                            xd = d["filter"][match]
                            variants = []
                            num_limits = xd.get("number", [0])

                            if not len(used_values) < num_limits[-1]:
                                enough = True

                            if param_or_value == "--":
                                variants = []
                            # should suggest also previous level
                            elif num_limits[0] == 0 or len(used_values) > num_limits[0]:
                                variants = list(
                                    set(allowed_keys) - set(used_keys)
                                )  # exclude used keys

                            # can suggest to input more values
                            if num_limits[-1] == 0 or len(used_values) < num_limits[-1]:
                                lookup_shell = xd.get("lookup_shell", [])

                                if not not lookup_shell:
                                    lookup_shell_exec = subprocess.run(
                                        lookup_shell, stdout=subprocess.PIPE, text=True
                                    )
                                    if lookup_shell_exec.returncode == 0:
                                        variants.append(
                                            lookup_shell_exec.stdout.strip()
                                        )

                            lookup_options = xd.get("lookup_options", [])

                            if len(lookup_options) > 0:
                                variants.extend(lookup_options)

                            if enough:
                                variants = []

                            if not variants:  # or not enough:
                                used_keys.append(path.pop())
                                break

                            rule1 = set(variants) - set(used_keys)
                            rule2 = rule1 - set(param_or_value)

                            res = list(rule2)

                            # if parameter definition is available,
                            # stop and print it
                            break
                        except (AttributeError, KeyError):
                            used_values.append(
                                path.pop()
                            )  # do not suggest the same value again

                            if len(path) > 0:
                                param_or_value = path[-1]

        print(clean_output(res) or "")


if __name__ == "__main__":
    main()
