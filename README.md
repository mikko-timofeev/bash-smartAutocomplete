# README

In some cases portable scripts have to be used but they lack a great autocompletion feature. This shell overlay provides ability to use autocompletion with portable scripts.

## Setup process

If Python `3.8` and `python38-ruamel.yaml` package are installed on a target computer no further setup is required (YAML presets are fully portable).
Check examples in [the "demos" folder](./demos).


## Preface

In this document arguments which require values are called 'parameters', arguments which do not are 'flags'.


## Configuration

Every argument (option/flag) can be configured with these two properties:
```
  order:
    # ARRAY
    # for parameters and flags
    # choose compatible version by regex in this order, if order is not defined, only '*' filter is used (no filtering), it acts as a whitelist, '*' is always evaluated if all 'order' items fail

  filter:
    # OBJECT (named array)
    # for defining filter_items
    # suggest only if target options were used, default: {} (no suggestions)
```

Every filter object can be configured with these three properties:
```
  number: [1]
    # ARRAY of integers
    # for parameters only
    # [min,max] amount of values required, default: [0 - optional, 0 - no limit]; examples: [1,0] (required at least one), [2] (exactly two values are required)

  lookup_shell: ['ls', './dumps']
    # ARRAY of strings
    # for parameters only
    # if set, uses this command to find matches (not used for flags), default: [] (no suggestions), examples: ['ls', './'] (suggest from the current directory)

  lookup_options: ['suggestion1', 'suggestion2', 'suggestion3', '--help']
    # ARRAY of strings
    # for parameters only
    # if set, uses this command to find matches (not used for flags), default: [] (no suggestions), examples: ['yes', 'no', 'idunno'] (suggest the choice of static string options)
```

Special case filters:
```
  --:
    # values suggested after all of the arguments
  ==:
    # key map for strict suggestions' hierarchy
```

The simplest flag declaration is an empty object:
```
param_pam_pam: {}
```


## Demo cases

```
  bash autocomplete ./demos/pacman.yml
  bash autocomplete ./demos/simple.yml
  ...
```

## Example

```
  bash autocomplete ./demos/simple.yml
    > sim<TAB>
    > simple <TAB>
    > simple arg<TAB>
    ? argA argB argC argD
    > simple arg
```


## Integration

To make your script autocompletable simply wrap it with `bash autocomplete` (configure autocompletion YAML file):
```
bash autocomplete ./custom_script.yml
```

Autocompletion YAML file can cover several targets (one or more commands).


---

# TODO:

* Cursor shifting (left-right) for editing
* Optimisation
* Autocompletion autobuilder from `--help` of original command/script (`bash autocomplete make my_helpful_script`)
* Combining single-letter flags
* Store runtime state in neat JSON object as opposed to various weird variables currently
* Rewrite all the terrible code into something beautiful
* Correct key *hold* handling
* Better autocompletion for suggested values
